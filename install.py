#!/usr/bin/env python

import argparse
import logging
import os
import shutil

LOG_FORMAT = "%(asctime)-15s %(levelname)s [%(threadName)s] %(message)s"
HOME = os.path.expanduser('~')

def get_args():
    parser = argparse.ArgumentParser()
    mods = ['mac', 'ubuntu', 'caddy', 'hammerspoon', 'nvim', 'git']
    parser.add_argument('--configure', action='store', choices=mods,
                            help="Configure scripts and configs")
    parser.add_argument('--user', action='store', help="User")
    parser.add_argument('--group', action='store', help="Group")
    parser.add_argument('--disable-vim-plugins', action='store_true',
                            help="prevent vim plugins installation")
    parser.add_argument('--forced', action='store_true', help="remove existing files and directories")
    parser.add_argument('--debug', action='store_true', help="set debug logging level")
    return parser.parse_args()

def remove_recursive(path):
    if os.path.isfile(path):
        os.remove(path)
    elif os.path.isdir(path):
        shutil.rmtree(path)
    elif os.path.is_symlink():
        os.unlink(path)
    else:
        raise OSError("unknown node type")


def traverse_and_appy(root, node_foo, leaf_foo):
    node_foo(root)
    for root, dirs, files in os.walk("path"):
        for d in dirs:
            node_foo(d)
        for f in files:
            leaf_foo(f)

def chown_recursive(path, user=None, group=None):
    if os.path.isdir(path):
        traverse_and_appy(path,
                            lambda p: shutil.chown(p, user, group),
                            lambda p: shutil.chown(p, user, group))
    else:
        shutil.chown(path, user, group)

def chmod_recursive(path, mod=None):
    if os.path.isdir(path):
        traverse_and_appy(path,
                            lambda p: os.chmod(path, mod),
                            lambda p: os.chmod(path, mod))
    else:
        os.chmod(path, mod)

def try_create(path, create_foo, forced=False, user=None, group=None, mod=None):
    if os.path.exists(path):
        if forced:
            remove_recursive(path)
        else:
            logging.warning('failed to create %s: path is already exists' % path)
            return False
    create_foo(path)
    if user is not None or group is not None:
        chown_recursive(path, user=user, group=group)
    if mod is not None:
        chmod_recursive(path, mod)
    return True

def try_create_file(path, forced=False, user=None, group=None, mod=None):
    return try_create(path, lambda p: open(p, 'x'), forced, user, group, mod)

def try_create_directory(path, forced=False, user=None, group=None, mod=None):
    return try_create(path, lambda p: os.mkdir(p), forced, user, group, mod)


def try_create_symlink(src, dst, forced=False):
    if os.path.exists(dst):
        if forced:
            remove_recursive(dst)
        else:
            logging.warning('failed to create link from %s to %s: dst path is already exists' % (src,dst))
            return False
    os.symlink(src, dst)
    return True


def try_copy(src, dst, forced=False, user=None, group=None, mod=None):
    if os.path.exists(dst):
        if forced:
            remove_recursive(dst)
        else:
            logging.warning('failed to copy %s to %s: dst path is already exists' % (src, dst))
            return False
    if os.path.isdir(src):
        shutil.copytree(src, dst)
    else:
        shutil.copy2(src, dst)
    if user is not None or group is not None:
        chown_recursive(dst, user=user, group=group)
    if mod is not None:
        chmod_recursive(dst, mod)
    return True


def configure(args):
    mode = args.configure
    def configure_nvim(install_plugins=True):
        logging.info("configuring nvim")
        vimrc_file = os.path.join(HOME, '.vimrc')
        nvim_dir = os.path.join(HOME, '.config', 'nvim')
        if args.disable_vim_plugins:
            vimrc_template = 'configs/.vimrc'
        else:
            vimrc_template = 'configs/.vimrc.with.plugins'
        try_copy(vimrc_template, vimrc_file, forced=args.forced, user=args.user, group=args.group)
        try_create_directory(os.path.join(HOME, '.vim'), forced=args.forced, user=args.user, group=args.group)
        try_create_directory(os.path.join(HOME, '.config'), forced=args.forced, user=args.user, group=args.group)
        try_create_symlink(os.path.join(HOME, '.vim'), nvim_dir)
        try_create_symlink(os.path.join(HOME, '.vimrc'), os.path.join(nvim_dir, 'init.vim'))
        #TODO add bundle here
    def configure_git():
        logging.info("configuring git")
        try_copy('configs/.gitconfig', os.path.join(HOME, '.gitconfig'),
                    forced=args.forced, user=args.user, group=args.group)
        try_copy('configs/.gitignore', os.path.join(HOME, '.gitignore'),
                    forced=args.forced, user=args.user, group=args.group)
    def configure_caddy():
        logging.info("configuring caddy")
        caddy_dir = '/etc/caddy'
        caddy_log_dir = '/var/log/caddy'
        try_create_directory(caddy_dir, forced=args.forced)
        try_copy('caddy/Caddyfile', os.path.join(caddy_dir, 'Caddyfile'),
                    forced=args.forced, user='www-data')
        try_copy('caddy/caddy.service', os.path.join(caddy_dir, 'caddy.service'),
                    forced=args.forced, user='www-data')
        try_create_symlink(os.path.join(caddy_dir, 'caddy.service'), '/etc/systemd/system/caddy.service',
                            forced=args.forced)
        try_create_directory(caddy_log_dir, forced=args.forced)
        #TODO: chown www-data:www-data
    def configure_hammerspoon():
        logging.info("configuring hammerspoon")
        try_copy('hammerspoon', os.path.join(HOME, '.hammerspoon'),
                    user=args.user, group=args.group)
    def configure_common():
        try_copy('configs/.bashrc', os.path.join(HOME, '.bashrc'),
                    forced=args.forced, user=args.user, group=args.group)
        try_copy('configs/.profile', os.path.join(HOME, '.profile'),
                    forced=args.forced, user=args.user, group=args.group)
        try_copy('configs/.pythonrc.py', os.path.join(HOME, '.pythonrc.py'),
                    forced=args.forced, user=args.user, group=args.group)
        try_copy('configs/.tmux.conf', os.path.join(HOME, '.tmux.conf'),
                    forced=args.forced, user=args.user, group=args.group)
    def configure_common_full():
        configure_common()
        configure_nvim(args.disable_vim_plugins)
        configure_git()

    if mode == 'mac':
        configure_common_full()
        configure_hammerspoon()
    elif mode == "ubuntu":
        configure_common_full()
    elif mode == "caddy":
        configure_caddy()
    elif mode == "hamerspoon":
        configure_hammerspoon()
    elif mode == "nvim":
        configure_git(args.disable_vim_plugins)
    elif mode == "git":
        configure_git()


def main(args):
    if args.configure is not None:
        configure(args)

if __name__ == "__main__":
    args = get_args()
    logging.basicConfig(format=LOG_FORMAT,
                        level=logging.DEBUG if args.debug else logging.INFO)
    main(args)

