local hotkey = require 'hs.hotkey'
local window = require 'hs.window'

local function module_init()
    local mash_full = config:get('fullscreen.mash', { "cmd", "ctrl", "alt", "shift" })
    local mash_default = config:get('fullscreen.mash', { "cmd", "ctrl", "alt" })
    local key = config:get('fullscreen.key', "return")

    hotkey.bind(mash_full, key, function()
        local win = window.focusedWindow()
        if win ~= nil then
            win:setFullScreen(not win:isFullScreen())
        end
    end)

    hotkey.bind(mash_default, key, function()
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local screen_frame = screen:frame()

        f.x = screen_frame.x
        f.y = screen_frame.y
        f.w = screen_frame.w
        f.h = screen_frame.h
        win:setFrame(f)
    end)
end

return {
    init = module_init
}
