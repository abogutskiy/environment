This repository contains scripts and configs which I use on my laptop and
developers servers.

## Content
 - configs – configs for linux/macos and standart must-have programs that
   can be installed from package manager
 - scripts – some useful scripts
 - services – services for standart programs, that somehow doesn't have a
 service script
 - caddy – all configuration files for [caddy](https://caddyserver.com/) web
   server
 - hammerspoon - configuration files and modules for a great macos
 [automation tool](https://www.hammerspoon.org/)
 - install.py – install scripts that can prepare places all the stuff above
   to its destination, sets permitions, changes ownership etc

## install.py

## Expected tools
1. nvim
2. git
3. tmux

## Caddy
To install and setup caddy on your machine:
- [install caddy](https://caddyserver.com/v1/download)
- run ```sudo ./install.py --configure caddy```
- deploy your site

All caddy configuration files are stored in */etc/caddy* folder. Caddy is ran
by user www-data and CADDYPATH is not set, so caddy assets are stored in
*/var/www/.caddy*. Daemon and access logs are written to stdout (jounals for
daemon). WebServer's html data is stored in /var/www/html folder.

If you want start caddy automatically at boot, just enable a new service:
```sudo systemctl enable caddy.service```

## Hammerspoon
To install and prepare hammerspoon on you machine (mac)
- [Download](https://github.com/Hammerspoon/hammerspoon/releases/tag/0.9.78)
  and move it to application folder.
- run ```sudo ./install.py --configure hammerspoon``` or copy hammerspon files to
  home folder ```cp -r environment/hammerspoon ~/.hammerspoon```

